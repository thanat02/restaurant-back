<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class RestaurantController extends Controller
{
    public function getRestaurants(Request $request)
    {
        //กำหนด req ที่รับจากหน้าบ้าน
        $validator = Validator::make($request->all(), [
            'search' => 'nullable|string'
        ]);

        //ถ้าค่าที่รับมาไม่ตรงตามที่กำหนดจะ return กลับออกไป
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //นำข้อมูลที่ผ่านการตรวจสอบแล้วออกมา
        $credentials = $validator->validated();

        $API_KEY = env('GOOGLE_MAPS_API_KEY');

        //สร้าง instance ของ Guzzle HTTP client
        $client = new Client();

        // ตรวจสอบว่ามีข้อมูลในแคชหรือไม่ ถ้ามีแล้วก็จะดึงข้อมูลออกมาแล้ว return กลับออกไป
        if (Cache::has($credentials['search'])) {
            return response()->json([
                'status' => true,
                'data' => Cache::get($credentials['search'])
            ], 200);
        }

        //ครอบ try-catch ในกรณีที่เกิด Exception
        try {
            //ส่งคำขอ api ไปยัง Google Places API เพื่อดึงข้อมูล lat,lng ของสถานที่
            $geoResponse = $client->get('https://maps.googleapis.com/maps/api/geocode/json', [
                'query' => [
                    'key' => $API_KEY,
                    'address' => $credentials['search']
                ]
            ]);
            //นำข้อมูลที่ได้กลับมา decode เพื่อนำมาใช้ต่อ
            $geoData = json_decode($geoResponse->getBody(), true);
            $lat = $geoData['results'][0]['geometry']['location']['lat'];
            $lng = $geoData['results'][0]['geometry']['location']['lng'];
            //ส่งคำขอ api ไปยัง Google Places API โดยการส่ง API_KEY และ คำค้นหา 
            $response = $client->get('https://maps.googleapis.com/maps/api/place/textsearch/json', [
                'query' => [
                    'key' => $API_KEY,
                    'location' => "$lat,$lng",
                    'radius' => 2000,
                    'type' => 'restaurant'
                ]
            ]);
            //นำข้อมูลที่ได้กลับมา decode เพื่อนำมาใช้ต่อ
            $data = json_decode($response->getBody(), true);

            $restaurants = $data['results'];

            //map ข้อมูลที่ได้มาเพื่อไป get รูปเพิ่มเติมและนำค่าออกมาแค่ที่ต้องการใช้
            $optimizedData = array_map(function ($restaurant) use ($API_KEY) {
                // $photoReference = $restaurant['photos'][0]['photo_reference'] ?? null;
                $image = null;

                // if ($photoReference) {
                //     //ดึงข้อมูลรูปที่ได้ออกมาเป็น base64
                //     $getImage = file_get_contents("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$photoReference&key=$API_KEY");
                //     $image = base64_encode($getImage);
                // }

                //ในกรณีที่ไม่มีรูปจะดึงรูปภาพจาก Storage มาใช้แทน
                if (!$image) {
                    $image_data = Storage::disk('public')->get('dream_TradingCard.jpg');
                    $image = base64_encode($image_data);
                }

                return [
                    'place_id'          => $restaurant['place_id'],
                    'name'              => $restaurant['name'],
                    'formatted_address' => $restaurant['formatted_address'],
                    'geometry'          => $restaurant['geometry'],
                    'photo'             => $image
                ];
            }, $restaurants);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'An error occurred: ' . $e->getMessage()
            ], 500);
        }

        // บันทึกข้อมูลลงในแคช และตั้งเวลาหมดอายุ
        Cache::put($credentials['search'], $optimizedData, 60);

        //response กลับออกไปเป็นรูปแบบ json
        return response()->json([
            'status' => true,
            'data' => $optimizedData
        ], 200);
    }
}
